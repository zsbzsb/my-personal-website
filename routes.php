<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// PAGES | HTTP GET requests only
//////////////////////////////////////////////////////////////////////////////////////////////////

// Home
$routes->map('GET', '/', array('source' => 'home.php', 'title' => 'Home', 'active' => 'home'), null);
$routes->map('GET', '/home', array('source' => 'home.php', 'title' => 'Home', 'active' => 'home'), 'home');

// Projects
$routes->map('GET', '/projects', array('source' => 'projects.php', 'title' => 'Projects', 'active' => 'projects'), 'projects');

// Project Page
$routes->map('GET', '/projects/[a:name]/[a:page]?', array('source' => 'project page.php', 'title' => 'Projects', 'active' => 'projects'), 'project_page');

// Contact
$routes->map('GET', '/contact', array('source' => 'contact.php', 'title' => 'Contact', 'active' => 'contact'), 'contact');

// Download
$routes->map('GET', '/download/[*:file]', array('source' => 'download.php', 'title' => 'Download', 'includetemplates' => false), 'download');

//////////////////////////////////////////////////////////////////////////////////////////////////
// API | HTTP POST requests only
//////////////////////////////////////////////////////////////////////////////////////////////////


?>

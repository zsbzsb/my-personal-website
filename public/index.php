<?php

// global defines
define('ROOT', realpath('..').'/');
define('SCRIPTROOT', ROOT.'scripts/');
define('TEMPLATEROOT', ROOT.'content/templates/');
define('PAGEROOT', ROOT.'content/pages/');
define('APIROOT', ROOT.'api/');
define('DOWNLOADROOT', ROOT.'downloads/');

// set timezone
date_default_timezone_set('UTC');

// setup routing
require SCRIPTROOT.'AltoRouter/AltoRouter.php';

$routes = new AltoRouter();

// load the actual routes
require ROOT.'routes.php';

// handle the route request
$route = $routes->match();

if (!$route)
{
  http_response_code(404);
  $error = 404;
  require PAGEROOT.'error.php';
  die();
}
else
{
  // get route target information
  $target = $route['target'];

  // all API requests will use a HTTP POST request method
  $apirequest = $_SERVER['REQUEST_METHOD'] == 'POST';

  // if it is an API request we will load all JSON variables from the request body into $_POST since php doesn't do this automatically
  if ($apirequest)
  {
    $_POST = json_decode(file_get_contents('php://input'), true);

    // also if the POST values contained the session id and it wasn't set in the headers we will set it here
    if (isset($_POST['token'])) $_COOKIE[$SESSION['tokenid']] = $_POST['token'];
  }

  // handle an API request
  if ($apirequest)
  {
    if (isset($target['postvariables']))
    {
      foreach ($target['postvariables'] as $variable)
      {
        if (!isset($_POST[$variable]))
        {
          http_response_code(400);
          $error = 400;
          require PAGEROOT.'error.php';
          die();
        }
        else
        {
          $$variable = htmlspecialchars(trim($_POST[$variable]));
        }
      }
    }

    if (isset($target['optionalvariables']))
    {
      foreach ($target['optionalvariables'] as $variable)
      {
        if (isset($_GET[$variable]))
        {
          $$variable = htmlspecialchars(trim($_POST[$variable]));
        }
      }
    }

    require SCRIPTROOT.'apiresponse.php';
    require APIROOT.$target['source'];
  }

  // handle a normal page request
  else
  {
    $includetemplates = isset($target['includetemplates']) ? $target['includetemplates'] : true;

    // extract any route parameters
    extract($route['params']);

    // page header and body
    if ($includetemplates) require TEMPLATEROOT.'header.php';
    if ($includetemplates)  require TEMPLATEROOT.'body.php';

    // page source
    require PAGEROOT.$target['source'];

    // page footer
    if ($includetemplates) require TEMPLATEROOT.'footer.php';
  }
}

?>

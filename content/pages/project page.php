<?php

$projects = json_decode(file_get_contents(PAGEROOT.'source/projects.json'), true);
if (isset($projects[$name])) $project = $projects[$name];
else
{
  echo 'Project was not found [404] :(';
  die();
}

echo '<h3 class="text-center">'.$project['name'].' - '.$project['header'].'</h3><br>';

if (!isset($page)) $page = 'about';

?>

<div class="row">
  <div class="col-md-2">
    <div class="panel panel-default">
      <div class="panel-heading"><?php echo $project['name']; ?></div>
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <?php

            foreach ($project['pages'] as $pagename => $pagevalue)
            {
              echo '<li'.($page == $pagename ? ' class="active"' : '').'><a href="'.$routes->generate('project_page', array('name' => $name, 'page' => $pagename)).'">'.$pagevalue['title'].'</a></li>';
            }

          ?>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-md-offset-1">
    <?php

      if (!isset($project['pages'][$page])) echo 'Page was not found [404] :(';
      else if ($page == 'downloads')
      {
        echo '
          <table class="table table-striped table-bordered">
           <thead>
             <tr>
               <th>Name</th>
               <th>Version</th>
               <th>Platform</th>
               <th>Compiler</th>
              </tr>
            </thead>
            <tbody>';

        foreach ($project['pages'][$page]['files'] as $filename => $fileinfo)
        {
          echo '<tr class="downloadfile" url="'.$routes->generate('download', array('file' => urlencode($filename))).'"><td>'.$filename.'</td><td>'.$fileinfo['version'].'</td><td>'.$fileinfo['platform'].'</td><td>'.$fileinfo['compiler'].'</td></tr>';
        }

        echo '</tbody></table>';
      }
      else echo $project['pages'][$page]['content'];

    ?>
  </div>
</div>

<script>

$('.downloadfile').click(function() {
  window.location.replace($(this).attr('url'));
});

</script>

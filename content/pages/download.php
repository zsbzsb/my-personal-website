<?php

$filename = DOWNLOADROOT.urldecode($file);
if(!file_exists($filename))
{
  header('location: /error');
  die();
}

$finfo = finfo_open(FILEINFO_MIME_TYPE);
header('Content-Type: '.finfo_file($finfo, $filename));
finfo_close($finfo);

header('Content-Disposition: attachment; filename="'.basename($filename).'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($filename));

ob_clean();
flush();
readfile($filename);

?>

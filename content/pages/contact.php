<h4 class="text-center">There are several ways to contact me, I have listed them below<br>so feel free to choose the one that is best for you.</h4>

<br>

<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="list-group">
      <a href="mailto:contact@zbrown.net" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/email.png" />
        Email - contact@zbrown.net
      </a>

      <a href="https://github.com/zsbzsb" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/github.png" />
        Github - https://github.com/zsbzsb
      </a>

      <a href="https://bitbucket.org/zsbzsb" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/bitbucket.png" />
        Bitbucket - https://bitbucket.org/zsbzsb
      </a>

      <a href="http://en.sfml-dev.org/forums/index.php?action=profile;u=5741" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/sfml.png" />
        SFML Forums - http://en.sfml-dev.org/forums
      </a>

      <a href="http://facebook.com/zachariahsbrown" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/facebook.png" />
        Facebook - http://facebook.com/zachariahsbrown
      </a>

      <a href="https://twitter.com/zachariahsbrown" target="_blank" class="list-group-item">
        <img alt="" target="_blank" src="/assets/images/twitter.png" />
        Twitter - https://twitter.com/zachariahsbrown
      </a>
    </div>
  </div>
</div>

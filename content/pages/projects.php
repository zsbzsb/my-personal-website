<div class="row">
  <div class="col-md-3 col-md-offset-3">
    <div class="thumbnail">
      <a href="<?php echo $routes->generate('project_page', array('name' => 'netext')); ?>"><img alt="" src="/assets/images/netext.png" /></a>
      <div class="caption">
        <h3>NetEXT</h3>
        <p>NetEXT is a SFML.NET extension library that is designed to provide extension modules to SFML for CLI/CLR/.NET languages. It currently covers areas in graphics, particles, and more.</p>
        <a href="<?php echo $routes->generate('project_page', array('name' => 'netext')); ?>" class="btn btn-info">Learn More >></a>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="thumbnail">
      <a href="<?php echo $routes->generate('project_page', array('name' => 'motion')); ?>"><img alt="" src="/assets/images/motion.png" /></a>
      <div class="caption">
        <h3>Motion</h3>
        <p>Motion is a complete video and audio player for SFML. It also supports SFML.NET with the MotionNET project. The API is straight forward so in just minutes you can get video playback up and running.</p>
        <a href="<?php echo $routes->generate('project_page', array('name' => 'motion')); ?>" class="btn btn-info">Learn More >></a>
      </div>
    </div>
  </div>
</div>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta -->
    <title>Zachariah's Place<?php if (isset($target['title'])) echo ' - '.$target['title']; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/assets/css/custom-theme.css" rel="stylesheet">

    <!-- JQuery -->
    <script src="/assets/js/jquery-2.1.1.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="/assets/js/bootstrap.min.js"></script>
  </head>

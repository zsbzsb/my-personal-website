<body>
  <nav class="navbar navbar-inverse navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Zachariah's Place</a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <?php $active = isset($target['active']) ? $target['active'] : ''; ?>
          <li<?php if ($active == 'home') echo ' class="active"'; ?>><a href="<?php echo $routes->generate('home'); ?>">Home</a></li>
          <li<?php if ($active == 'projects') echo ' class="active"'; ?>><a href="<?php echo $routes->generate('projects'); ?>">Projects</a></li>
          <li<?php if ($active == 'contact') echo ' class="active"'; ?>><a href="<?php echo $routes->generate('contact'); ?>">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container" id="contentarea">
